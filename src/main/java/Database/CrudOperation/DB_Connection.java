package Database.CrudOperation;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;

public class DB_Connection {
	public Connection get_connection() {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String userName = "dev_test_dbo";
			String password = "dev_test_dbo123";
			String url = "jdbc:sqlserver://vNTDACWSSQLD002:1433;databaseName=DEV_TEST";
			connection = DriverManager.getConnection(url, userName, password);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}
}
