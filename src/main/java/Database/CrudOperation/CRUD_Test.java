package Database.CrudOperation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class CRUD_Test {
      public void create_data(String id,String name,String email) {
    	  DB_Connection obj_DB_Connection = new DB_Connection();
    	  Connection connection=obj_DB_Connection.get_connection();
    	  
    	  PreparedStatement ps=null;
    	  try {
    		  String query="insert into asifcrud(id,name,eamil) values(?,?,?)";
    		  ps=connection.prepareStatement(query);
    		  ps.setString(1, id);
    		  ps.setString(2, name);
    		  ps.setString(3, email);
    		  System.out.println(ps);
    		  ps.executeUpdate();
    		  
    		  
    	  }catch(Exception e) {
    		  System.out.println(e);
    	  }
      }
      public void read_data(String id){
    		DB_Connection obj_DB_Connection=new DB_Connection();
    		Connection connection=obj_DB_Connection.get_connection();
    		PreparedStatement ps=null;
    		ResultSet rs=null;
    		try {
    			String query="select * from asifcrud";
    			ps=connection.prepareStatement(query);
    			//ps.setString(1, sl_no);
    			System.out.println(ps);
    			rs=ps.executeQuery();
    			while(rs.next()){
    			System.out.println("id -"+rs.getString("id"));
    			System.out.println("name -"+rs.getString("name"));
    			System.out.println("email -"+rs.getString("eamil"));
    			System.out.println("---------------");
    			}
    		} catch (Exception e) {
    			System.out.println(e);
    		}
    	}
    
	public void update_data(String id, String new_id, String name, String eamil) {
		DB_Connection obj_DB_Connection=new DB_Connection();
		Connection connection=obj_DB_Connection.get_connection();
		PreparedStatement ps=null;
		try {
			String query="update asifcrud set id=?,name=?,eamil=? where id=?";
			ps=connection.prepareStatement(query);
			ps.setString(1, new_id);
			ps.setString(2, name);
			ps.setString(3, eamil);
			ps.setString(4, id);
			System.out.println(ps);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	public void delete_data(String id) {
		DB_Connection obj_DB_Connection=new DB_Connection();
		Connection connection=obj_DB_Connection.get_connection();
		PreparedStatement ps=null;
		try {
			String query="delete from asifcrud where id=?";
			ps=connection.prepareStatement(query);
			ps.setString(1, id);
			System.out.println(ps);
			ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
		
}
