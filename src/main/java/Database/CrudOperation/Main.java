package Database.CrudOperation;

public class Main {

	public static void main(String[] args) {
		DB_Connection obj_DB_Connection = new DB_Connection();
		System.out.println(obj_DB_Connection.get_connection());
		System.out.println();
		CRUD_Test obj1 = new CRUD_Test();
		obj1.create_data("3","Minhaz Rahman","minhaz@gmail.com");
		System.out.println();
		obj1.read_data("1");
		obj1.update_data("1","11","Asif hasan","asifhasan014@gmail.com");
        System.out.println();
        obj1.delete_data("3");
	}

}
